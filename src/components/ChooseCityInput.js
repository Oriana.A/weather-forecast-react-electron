import React from 'react';

class ChooseCityInput extends React.Component{
    render(){
    return (
        <div className="choose-city-input">
            <input className="choosen-city" type="search" value={this.props.value} onChange={this.props.onChange}></input>
        </div>
    );
}
}

export default ChooseCityInput;
