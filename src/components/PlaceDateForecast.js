import React from 'react';
import { connect } from 'react-redux';

class PlaceDateForecast extends React.Component{
    render(){
        return (
            <div className="place-date-forecast">
                <header className="header" >
                  <h1 className="city">{this.props.weatherForecast.city}</h1>
                  <h3 className="datetime">{this.props.weatherForecast.localtime}</h3>
                </header>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        weatherForecast:state.forecastReducer.weatherForecast,
    }
}


export default connect(mapStateToProps)(PlaceDateForecast)
